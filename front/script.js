
jQuery(document).ready(function ($) {
    /**
     * Plugin wp-one-time-popup.
     * Global object is "wp_one_time_popup".
     */
    "use strict";

    // Because of html cache this script could be loaded,
    // so must also check here if cookie is set.
    if( getCookie( wp_one_time_popup.cookie_name ) )
        return ;
    
    $(wp_one_time_popup.html).appendTo('body');

    var $modal = $('#wp-one-time-popup-modal')
        .show();

    $('.close', $modal).on('click', function () {
        $modal.hide();
    });
    $('.consent button', $modal).on('click', function () {
        $.post(wp_one_time_popup.ajax_url, {
            _ajax_nonce: wp_one_time_popup.ajax_nonce,
            action: wp_one_time_popup.action_consent,
        }, function (data) {
            $modal.hide();
        }
        );
    });

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return null ;
      }
});
