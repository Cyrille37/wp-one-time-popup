<?php
/**
 * One Time Popup
 *
 * @package           OneTimePopup
 * @author            Cyrille37
 * @copyright         2023 Cyrille37
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       One Time Popup
 * Plugin URI:        https://framagit.org/Cyrille37/wp-one-time-popup
 * Description:       Description of the plugin.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Your Name
 * Author URI:        https://framagit.org/Cyrille37
 * Text Domain:       plugin-slug
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Update URI:        https://framagit.org/Cyrille37/wp-one-time-popup
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//error_log(__METHOD__ . ' ' . $_SERVER['REQUEST_URI']);
//error_log(__METHOD__.' is_admin:'.(is_admin()?'True':'False') );
//error_log(__METHOD__.' is_blog_admin:'.(is_blog_admin()?'True':'False') );

if( is_blog_admin() )
{
	require_once(__DIR__.'/src/OneTimePopupAdmin.php');
	new OneTimePopupAdmin();
}
else
{
	require_once(__DIR__.'/src/OneTimePopupFront.php');
	new OneTimePopupFront();
}
