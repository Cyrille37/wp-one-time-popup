��          T      �       �      �      �   {   �      P  k   Y  H   �  �       �     �  �   �  	   j  o   t  �   �                                        Button Cookie time to live Display a Popup until the user click on the button. The click set a Cookie that will hide the Popup until Duration setting. Settings The cookie life time in seconds (One month = %d).<br/>Current value is <span class="current-value"></span>. You can use "$PRIVACY_URL$" to create a link to the Privacy Policy Page. Project-Id-Version: 
PO-Revision-Date: 2023-05-03 11:11+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ../src
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: ../languages
X-Poedit-SearchPathExcluded-1: ../.git
 Bouton Durée de vie du Cookie Affiche une Popup jusqu'à ce que l'utilisateur clique sur le bouton. Le clic dépose un cookie qui cachera la Popup pour la durée indiquée. Réglages La durée de vie du cookie en secondes (Un mois = %d).<br/>Valeur courante <span class="current-value"></span>. Vous pouvez utiliser le mot clé "$PRIVACY_URL$" qui sera remplacer par le titre et le lien de la page de politique de confidentialité. 