<?php

abstract class OneTimePopup
{
    const PLUGIN_NAME = 'wp-one-time-popup';
    const PLUGIN_VERSION = '1.0';

    const OPTION_NAME = self::PLUGIN_NAME.'_options' ;

    const OPTION_FIELD_MESSAGE = 'message';
    const OPTION_FIELD_BUTTON = 'button';
    const OPTION_FIELD_DURATION = 'duration';
    const OPTION_FIELD_COOKIENAME = 'cookie';

    const OPTION_FIELD_COOKIENAME_DEFAULT = self::PLUGIN_NAME ;
    const OPTION_FIELD_DURATION_DEFAULT = MONTH_IN_SECONDS * 3 ;

    public $plugin_path;
    public $plugin_url;

    public function __construct()
    {
        $this->plugin_path = realpath( __DIR__.'/..' ) ;
        $this->plugin_url = plugins_url(self::PLUGIN_NAME);
    }

    public static function get_privacy_policy_page_data()
    {
        static $data ;
        if( $data )
            return $data ;

        $id = (int) get_option( 'wp_page_for_privacy_policy' );
        /**
         * @var WP_Post $post
         */
        $post = get_post($id);
        $data = [
            'id' => $id ,
            'title' => $post ? $post->post_title : null,
            'url' => $post ? get_page_uri( $post ) : null,
        ];
        return $data ;
    }
}
