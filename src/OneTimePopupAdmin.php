<?php

require_once(__DIR__ . '/OneTimePopup.php');

/**
 * Settings page generator
 * https://jeremyhixon.com/tool/wordpress-option-page-generator/
 */
class OneTimePopupAdmin extends OneTimePopup
{

    const PAGE = self::PLUGIN_NAME;
    const OPTION_GROUP = self::PLUGIN_NAME . '_settings';
    const OPTION_SECTION1 = self::PLUGIN_NAME . '_section1';

    protected $options;

    public function __construct()
    {
        parent::__construct();

        add_action('admin_init', [$this, 'settings_init']);
        add_action('admin_menu', function () {
            add_options_page('One Time Popup', 'One Time Popup', 'manage_options', self::PAGE, [$this, 'page']);
        });
        add_action('admin_enqueue_scripts', [$this, 'wp_enqueue_scripts']);
    }

    public function wp_enqueue_scripts()
    {
        wp_register_style(self::PLUGIN_NAME . '-css', $this->plugin_url . '/admin/style.css');
        wp_enqueue_style(self::PLUGIN_NAME . '-css');

        wp_register_script(self::PLUGIN_NAME . '-js', $this->plugin_url . '/admin/script.js', array('jquery'), self::PLUGIN_VERSION, true);
        wp_enqueue_script(self::PLUGIN_NAME . '-js');
    }

    public function page()
    {
        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
        }

        $this->options = get_option(self::OPTION_NAME);

        // add error/update messages

        // check if the user have submitted the settings
        // WordPress will add the "settings-updated" $_GET parameter to the url
        if (isset($_GET['settings-updated'])) {
            // add settings saved message with the class of "updated"
            //add_settings_error('wporg_messages', 'wporg_message', __('Settings Saved', self::PLUGIN_NAME), 'updated');
        }

        // show error/update messages
        //settings_errors('wporg_messages');

?>
        <div class="wrap">
            <h2><?php echo esc_html(get_admin_page_title()); ?></h2>

            <form action="options.php" method="post">
                <?php
                settings_fields(self::PLUGIN_NAME . '_settings');
                do_settings_sections(self::PLUGIN_NAME . '_settings');
                submit_button('Save Settings');
                ?>
            </form>
        </div>
<?php
    }

    public function settings_init()
    {
        load_plugin_textdomain(self::PLUGIN_NAME, false, self::PLUGIN_NAME . '/languages');

        // Register a new setting for "wporg" page.
        register_setting(self::OPTION_GROUP, self::OPTION_NAME, [$this, 'sanitize']);

        // Register a new section in the "wporg" page.
        add_settings_section(
            self::OPTION_SECTION1,
            __('Settings', self::PLUGIN_NAME),
            [$this, 'render_section'],
            self::OPTION_GROUP,
            [
                'text' => __('Display a Popup until the user click on the button. The click set a Cookie that will hide the Popup until Duration setting.', self::PLUGIN_NAME),
            ]
        );

        add_settings_field(
            // As of WP 4.6 this value is used only internally.
            // Use $args' label_for to populate the id inside the callback.
            self::OPTION_FIELD_MESSAGE,
            __('Message', self::PLUGIN_NAME),
            [$this, 'render_editor'],
            self::OPTION_GROUP,
            self::OPTION_SECTION1,
            [
                'name' => self::OPTION_FIELD_MESSAGE,
                'help' => __('You can use "$PRIVACY_URL$" to create a link to the Privacy Policy Page.', self::PLUGIN_NAME),
            ]
        );

        add_settings_field(
            self::OPTION_FIELD_BUTTON,
            __('Button', self::PLUGIN_NAME),
            [$this, 'render_text'],
            self::OPTION_GROUP,
            self::OPTION_SECTION1,
            [
                'name' => self::OPTION_FIELD_BUTTON,
            ]
        );

        add_settings_field(
            self::OPTION_FIELD_DURATION,
            __('Cookie time to live', self::PLUGIN_NAME),
            [$this, 'render_int'],
            self::OPTION_GROUP,
            self::OPTION_SECTION1,
            [
                'name' => self::OPTION_FIELD_DURATION,
                'default' => self::OPTION_FIELD_DURATION_DEFAULT,
                'help' => sprintf(
                    __('The cookie life time in seconds (One month = %d).<br/>Current value is <span class="current-value"></span>.', self::PLUGIN_NAME),
                    MONTH_IN_SECONDS
                ),
            ]
        );

        add_settings_field(
            self::OPTION_FIELD_COOKIENAME,
            __('Cookie name', self::PLUGIN_NAME),
            [$this, 'render_text'],
            self::OPTION_GROUP,
            self::OPTION_SECTION1,
            [
                'name' => self::OPTION_FIELD_COOKIENAME,
                'default' => self::OPTION_FIELD_COOKIENAME_DEFAULT,
            ]
        );
    }

    function sanitize($input)
    {
        //error_log(__METHOD__ . ' input:' . var_export($input, true));

        $sanitized = [];

        if (isset($input[self::OPTION_FIELD_BUTTON])) {
            $sanitized[self::OPTION_FIELD_BUTTON] = sanitize_text_field($input[self::OPTION_FIELD_BUTTON]);
        }
        if (isset($input[self::OPTION_FIELD_MESSAGE])) {
            $sanitized[self::OPTION_FIELD_MESSAGE] = wp_kses_post($input[self::OPTION_FIELD_MESSAGE]);
        }

        if (isset($input[self::OPTION_FIELD_COOKIENAME])) {
            $sanitized[self::OPTION_FIELD_COOKIENAME] = sanitize_html_class($input[self::OPTION_FIELD_COOKIENAME]);
        } else
            $sanitized[self::OPTION_FIELD_COOKIENAME] = self::OPTION_FIELD_COOKIENAME_DEFAULT;

        if (isset($input[self::OPTION_FIELD_DURATION]) && !empty($input[self::OPTION_FIELD_DURATION])) {
            $sanitized[self::OPTION_FIELD_DURATION] = intval($input[self::OPTION_FIELD_DURATION]);
        } else
            $sanitized[self::OPTION_FIELD_DURATION] = self::OPTION_FIELD_DURATION_DEFAULT;

        return $sanitized;
    }

    function render_section($args)
    {
        //error_log(__METHOD__.' args:'.var_export($args,true) );
        printf(
            '<p id="%s">%s</p>',
            esc_attr($args['id']),
            esc_html($args['text'])
        );
    }

    function render_int($args)
    {
        //error_log(__METHOD__.' args:'.var_export($args,true) );
        $name = $args['name'];
        $default_value = isset($args['default']) ? $args['default'] : null ;

        printf(
            '<input class="regular-text" type="number" name="%s[%s]" id="%s" min="%d" max="%d" value="%s" />',
            self::OPTION_NAME,
            $name,
            self::OPTION_NAME . '_' . $name,
            isset($args['min']) ? intval($args['min']) : 0,
            isset($args['max']) ? intval($args['max']) : PHP_INT_MAX,
            isset($this->options[$name]) ? intval($this->options[$name]) : $default_value
        );
        if (isset($args['help']))
            printf(
                '<p class="%s description">%s</p>',
                self::OPTION_NAME . '_' . $name,
                //esc_html($args['help'])
                $args['help']
            );
    }

    function render_text($args)
    {
        //error_log(__METHOD__.' args:'.var_export($args,true) );
        $name = $args['name'];
        $default_value = isset($args['default']) ? $args['default'] : null ;

        printf(
            '<input class="regular-text" type="text" name="%s[%s]" id="%s" value="%s" />',
            self::OPTION_NAME,
            $name,
            self::OPTION_NAME . '_' . $name,
            isset($this->options[$name]) ? esc_attr($this->options[$name]) : $default_value
        );
        if (isset($args['help']))
            printf(
                '<p class="%s description">%s</p>',
                self::OPTION_NAME . '_' . $name,
                //esc_html($args['help'])
                $args['help']
            );
    }

    function render_textarea($args)
    {
        //error_log(__METHOD__.' args:'.var_export($args,true) );
        $name = $args['name'];
        $default_value = isset($args['default']) ? $args['default'] : null ;

        printf(
            '<textarea class="regular-text" type="text" name="%s[%s]" id="%s">%s</textarea>',
            self::OPTION_NAME,
            $args['name'],
            self::OPTION_NAME . '_' . $name,
            isset($this->options[$name]) ? esc_attr($this->options[$name]) : $default_value
        );
        if (isset($args['help']))
            printf(
                '<p class="%s description">%s</p>',
                self::OPTION_NAME . '_' . $name,
                //esc_html($args['help'])
                $args['help']
            );
    }

    function render_editor($args)
    {
        //error_log(__METHOD__.' args:'.var_export($args,true) );
        $name = $args['name'];
        $default_value = isset($args['default']) ? $args['default'] : null ;

        echo wp_editor(
            $this->options[$name],
            self::OPTION_NAME . '_' . $name,
            [
                'textarea_name' => self::OPTION_NAME . '[' . $name . ']',
                'textarea_rows' => 10,
                'teeny' => false,
                'wpautop' => false, // keeps "<p>"
                'media_buttons' => false
            ]
        );
        if (isset($args['help']))
            printf(
                '<p class="%s description">%s</p>',
                self::OPTION_NAME . '_' . $name,
                //esc_html($args['help'])
                $args['help']
            );
    }

}
