<?php

require_once(__DIR__ . '/OneTimePopup.php');

class OneTimePopupFront extends OneTimePopup
{
    const COOKIE_NAME = self::PLUGIN_NAME;

    const AJAX_ACTION_CONSENT = self::PLUGIN_NAME . '-consent';

    protected $options;
    protected $consented;

    public function __construct()
    {
        // Do not load if other's stuff
        if ($this->ignoreRequest())
            return;

        parent::__construct();

        $this->options = get_option(self::OPTION_NAME);

        // Do not load if options are not set
        if (!isset($this->options[self::OPTION_FIELD_COOKIENAME]) || empty($this->options[self::OPTION_FIELD_COOKIENAME])) {
            return;
        }
        // Do not load if options are not set
        if (!isset($this->options[self::OPTION_FIELD_BUTTON]) || empty($this->options[self::OPTION_FIELD_BUTTON])) {
            return;
        }
        // Do not load if options are not set
        if (!isset($this->options[self::OPTION_FIELD_MESSAGE]) || empty($this->options[self::OPTION_FIELD_MESSAGE])) {
            return;
        }

        add_action('init', [$this, 'wp_init']);
        add_action('wp_enqueue_scripts', [$this, 'wp_enqueue_scripts']);

        // Must add ajax for 2 ways because of page builder like Divi
        add_action('wp_ajax_nopriv_' . self::PLUGIN_NAME . '-consent', [$this, 'action_consent']);
        add_action('wp_ajax_' . self::PLUGIN_NAME . '-consent', [$this, 'action_consent']);
    }

    /**
     * Set consented True or False.
     */
    public function wp_init()
    {
        /*
        error_log(__METHOD__.' is_front_page:'.(is_front_page()?'True':'False') );
        error_log(__METHOD__.' is_preview:'.(is_preview()?'True':'False') );
        error_log(__METHOD__.' is_user_logged_in:'.(is_user_logged_in()?'True':'False') );
        error_log(__METHOD__.' is_user_admin:'.(is_user_admin()?'True':'False') );
        */

        if (isset($_COOKIE[$this->options[self::OPTION_FIELD_COOKIENAME]]))
            $this->consented = true;
        else
            $this->consented = false;
    }

    public function wp_enqueue_scripts()
    {
        if ($this->consented) {
            return;
        }

        wp_register_style(self::PLUGIN_NAME . '-css', $this->plugin_url . '/front/style.css');
        wp_enqueue_style(self::PLUGIN_NAME . '-css');

        wp_register_script(self::PLUGIN_NAME . '-js', $this->plugin_url . '/front/script.js', array('jquery'), self::PLUGIN_VERSION, true);
        wp_enqueue_script(self::PLUGIN_NAME . '-js');

        $html = file_get_contents($this->plugin_path . '/front/popup.html');
        //$options = get_option(self::OPTION_NAME);
        $html = str_replace('$BUTTON$', $this->options[self::OPTION_FIELD_BUTTON], $html);
        $html = str_replace('$MESSAGE$', $this->options[self::OPTION_FIELD_MESSAGE], $html);

        $privacy_policy_page_data = self::get_privacy_policy_page_data();
        if (!empty($privacy_policy_page_data['url']))
            $html = str_replace('$PRIVACY_URL$', sprintf(
                '<a href="%s">%s</a>',
                $privacy_policy_page_data['url'],
                $privacy_policy_page_data['title'],
            ), $html);

        wp_localize_script(
            self::PLUGIN_NAME . '-js',
            str_replace('-', '_', self::PLUGIN_NAME),
            array(
                'ajax_url' => admin_url('admin-ajax.php'),
                'ajax_nonce' => wp_create_nonce(self::PLUGIN_NAME),
                'plugin_name' => self::PLUGIN_NAME,
                'action_consent' => self::AJAX_ACTION_CONSENT,
                'html' => $html,
                'cookie_name' => $this->options[self::OPTION_FIELD_COOKIENAME],
            )
        );
    }

    public function action_consent()
    {
        // Check nonce
        check_ajax_referer(self::PLUGIN_NAME);
        echo 'welcome';
        //$duration = 60 * 60 * 24 * 30 * 3;
        //$duration = MONTH_IN_SECONDS * 3;
        $duration = $this->options[self::OPTION_FIELD_DURATION];
        setcookie($this->options[self::OPTION_FIELD_COOKIENAME], '1', time() + $duration, '/');
        wp_die();
    }

    protected function ignoreRequest()
    {
        //error_log(__METHOD__ . ' ' . $_SERVER['REQUEST_URI']);

        if (defined('DOING_CRON') && DOING_CRON)
            return true;

        if (defined('DOING_AJAX') && DOING_AJAX && isset($_REQUEST['action'])) {
            switch ($_REQUEST['action']) {

                case self::AJAX_ACTION_CONSENT:
                    return false;

                case 'heartbeat':
                case 'closed-postboxes':
                case 'health-check-site-status-result':
                case 'oembed-cache':
                default:
                    return true;
            }
            //error_log(__METHOD__ . ' ajax action:' . $_REQUEST['action']);
        }

        $uri = $_SERVER['REQUEST_URI'];
        if (strlen($uri) > 5 && strpos($uri, '.map', -5) !== false)
            return true;

        return false;
    }
}
