=== One Time Popup ===
Contributors: Cyrille37
Donate link: https://framagit.org/Cyrille37
Tags: popup
Requires at least: 6.0
Tested up to: 6.0
Stable tag: 1.0
Requires PHP: 7.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Manage a popup

